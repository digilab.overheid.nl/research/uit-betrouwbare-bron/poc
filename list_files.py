import os
import re

# Define paths for the source and target repositories
directory = "./content/documentatie"

def extract_title_from_md(file_path):
    """
    Extract the title and other metadata tags from the given markdown (.md) file.
    """
    try: 
        with open(file_path, 'r') as f:
            lines = f.readlines()
            # Check if the file contains YAML front matter (starts with '---')
            if lines[0].strip() == "---":
                front_matter = []
                for line in lines[1:]:
                    if line.strip() == "---":
                        break
                    front_matter.append(line.strip())  # Store each line of front matter
                
                # Parse the front matter manually
                metadata = {}
                for line in front_matter:
                    if ':' in line:
                        key, value = line.split(':', 1)  # Split only on the first colon
                        metadata[key.strip()] = value.strip().lower()  # Store key-value pairs
                
                # Look for the 'title' tag in the metadata
                title_match = metadata.get('title')
                category = metadata.get('category', 'No category')
                navigation = metadata.get('navigation', 'None')
                draft = metadata.get('draft', 'false')
                preliminary = metadata.get('preliminary', 'false')

                if title_match:
                    return title_match.strip(), category, navigation, draft, preliminary
                else:
                    print(f"No title found in front matter in {file_path}")
                return None
    except FileNotFoundError:
        print(f"Error: The file at {file_path} was not found.")
        return None
    except Exception as e:
        print(f"Error reading file {file_path}: {e}")
        return None       
        
        
def create_summary_of_titles(directory):
    """
    Create a summary of .md files with their titles from the specified directory,
    ordered numerically by the filename.
    """
    titles = []

    # Iterate over all files in the given directory
    for filename in os.listdir(directory):
        # print(f"Processing file: {filename}")
        if filename.endswith('.md'):
            file_path = os.path.join(directory, filename)
            result = extract_title_from_md(file_path)
            if result:
                title, category, navigation, draft, preliminary = result
                titles.append((filename, title, category, navigation, draft, preliminary))

    # Sort the files numerically by filename
    titles.sort(key=lambda x: [int(part) if part.isdigit() else part for part in re.split(r'(\d+)', x[0])])

    # Write the sorted titles to a new markdown file
    summary_file = os.path.join(directory, 'overzicht_documentatie_bestanden.md')
    print(f"Writing summary to {summary_file}")
    with open(summary_file, 'w', encoding='utf-8') as summary:
        summary.write("# Overzicht documentatiebestanden\n\n")
        summary.write(f"| **Bestandsnaam** | **Category** | **Title** | **Navigation** | **Draft** | **Preliminary** |\n")
        summary.write(f"| --- | --- | --- | --- | --- | --- |\n")
        for filename, title, category, navigation, draft, preliminary in titles:
            summary.write(f"| **{filename}** | {category} | {title} | {navigation} | {draft} | {preliminary} |\n")

    print(f"Summary of titles written to: {summary_file}")

if __name__ == "__main__":
    create_summary_of_titles(directory)
else:
    print("No changed files detected.")
