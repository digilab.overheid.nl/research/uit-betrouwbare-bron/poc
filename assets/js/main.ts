// Sidebar toggle. Note: the menu toggle is implemented using pure CSS with a hidden checkbox
document.getElementById('sidebar-toggle')?.addEventListener('click', () => {
  document.body.classList.toggle('show-sidebar');
});

// Close the menu and sidebar on backdrop click
const menuInputEl = document.getElementById('menu-input') as HTMLInputElement;
for (const el of document.querySelectorAll('#backdrop, #sidebar-close')) {
  el.addEventListener('click', () => {
    menuInputEl.checked = false;
    document.body.classList.remove('show-sidebar');
  });
}


// Search suggestions for the search input, using FlexSearch
import flexsearch from 'flexsearch';
// import advanced from 'flexsearch/dist/lang/latin/advanced.min';

const index = new flexsearch.Document({
  document: {
    id: 'id',
    index: [
      {
        field: 'title',
        // tokenize: 'forward',
        tokenize: 'forward', // IMPROVE: also match partially, similar to VSCode file search?
        // encode: advanced, // IMPROVE: add a (language-specific) encoder?
        cache: true, // Note: similar to that used in https://github.com/nextapps-de/flexsearch/blob/master/demo/autocomplete.html. IMPROVE: add bounds/limit?
      },
      {
        field: 'description',
        tokenize: 'forward',
        cache: true,
      },
      {
        field: 'content',
        tokenize: 'forward',
        cache: true,
      },
    ],
  },
});

// Read the content for the search suggestions. Note: wrapped inside a function, since Hugo does not support top level await. IMPROVE: insert the data directly in this file, like the Doks theme does?
(async () => {
  // {{ $searchData := resources.Get "js/search-data.json" | resources.ExecuteAsTemplate "js/search-data.json" . }}
  // {{ if hugo.IsProduction }}
  //   {{ $searchData = $searchData | fingerprint }}
  // {{ end }}
  let data: any;

  try {
    const response = await fetch('{{ $searchData.RelPermalink }}');
    if (!response.ok) {
      throw new Error('Error loading search suggestions');
    }

    data = await response.json();
  } catch (error) {
    console.error('Error fetching the JSON file:', error);
    throw error;
  }

  for (let i = 0; i < data.length; i++) {
    index.add(i, data[i]); // Note: use the index instead of the ID as key
  }

  const searchInput = document.getElementById('search-input') as HTMLInputElement;
  const searchResults = document.getElementById('search-results') as HTMLElement;
  const searchForm = document.getElementById('search-form') as HTMLInputElement;

  document.getElementById('search-form')?.addEventListener('submit', (e) => { e.preventDefault(); });

  searchInput.addEventListener('input', () => {
    let suggestions: any[] = [];

    if (searchInput.value) {
      const results = index.search(searchInput.value, { limit: 10, suggest: true });

      // For now, only take the first match into account (match by title, or if that does not match: by description, or otherwise: by content)
      if (results.length) {
        suggestions = results[0].result.map(i => data[i]); // Note: instead of a loop with direct array element assignment, which is ignored by Svelte reactivity
      }
    }

    searchResults.replaceChildren(...suggestions.map(sug => {
      const el = document.createElement('a');
      el.setAttribute('href', sug.href);
      const title = document.createElement('span');
      title.textContent = sug.title;
      const description = document.createElement('span');
      description.textContent = sug.description;

      el.appendChild(title);
      el.appendChild(description);
      return el;
    }));
  });

  // Close the search suggestions when clicked outside the search form (which contains the input and search results)
  document.body.addEventListener('click', (event: MouseEvent) => {
    // If the clicked element is the search form or a descendant of it, clear the search suggestions
    if (!searchForm.contains(event.target as Node)) {
      searchResults.replaceChildren();
    }
  });
})();

// Update the sidebar active item when scrolling
const sidebar = document.getElementById('sidebar');

if (sidebar) {
  const showAsActive = (targetID: string) => {
    // Clear all 'active' classes in the sidebar
    for (let el of sidebar.querySelectorAll('.current .active')) {
      el.classList.remove('active');
    }

    // Set the link thats links to the targetID as active
    if (targetID) {
      sidebar.querySelector(`.current [href$="#${targetID}"]`)?.classList.add('active');
    } else {
      // If no active page selected, set the current page as active
      sidebar.querySelector(`.current a`)?.classList.add('active'); // Note: querySelector matches the first element that matches the selector
    }
  };

  const headings = document.querySelectorAll('article h1, article h2');
  const headingIDs = Array.from(headings).map(el => el.id);
  let previousY = window.scrollY;
  let previousIntersecting: string;

  let observer = new IntersectionObserver((entries) => {
    for (let { isIntersecting, target } of entries) {
      const isScrollingUp = window.scrollY < previousY;
      previousY = window.scrollY;

      if (isIntersecting) {
        // First intersecting element: show as active, then return
        showAsActive(target.id);
        previousIntersecting = target.id;
        return;
      }

      // Else (when this heading scrolls out of the root)...
      if (isScrollingUp) {
        // When leaving this element while scrolling up: set the heading above the current one as active
        const targetIndex = headingIDs.indexOf(target.id);
        if (targetIndex <= 0) {
          showAsActive('');
        } else {
          showAsActive(headingIDs[targetIndex - 1]);
        }
      }
    }
  }, {
    // root: null, // If unspecified or null: browser viewport
    rootMargin: '16px 0px -80%', // Corresponds to the header height, see also the scroll-margin-top in main.scss. Must be specified in pixels and/or percents. -80% used to only trigger when the heading is in the top part of the viewport
    threshold: 0, // Trigger when at least 1 pixel of the content is inside the root/viewport
  });

  for (const heading of headings) {
    observer.observe(heading);
  }
}
