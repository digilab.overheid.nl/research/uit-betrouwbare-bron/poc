---
title: Ontwerpkeuzes
category: proof of concept
description: "Ontwerpkeuzes voor het informatiemodel, gebaseerd op de scope en inzichten van Uit betrouwbare bron"
type: 
navigation:
preliminary: true
draft: false
layout: single
weight: 1
---

## Inleiding

De scope-bepaling van de proof of concept brengt een beperking van het informatiemodel IMWOZ met zich mee. De gegevens die in de proof of concept zijn al gemodelleerd in het IMWOZ, maar het IMWOZ omvat meer objecten dan in deze proof of concept nodig zijn. 

In het huidige IMWOZ zijn ontwerpkeuzes gemaakt die te maken hebben met het (deels) implementeren van aspecten zoals historie en annotaties (in onderzoek). Vanwege het karakter van Uit betrouwbare bron zal het informatiemodel op deze onderwerpen aangepast worden. 

Ook zijn in het IMWOZ objecten opgenomen uit andere registraties zoals personen en kadastrale onroerende zaken. In het kader van "Data bij de bron" zullen hier ook ontwerpkeuzes op worden gemaakt. Deze ontwerpkeuzes zullen gedocumenteerd worden en onderbouwd worden. 

Daarbij wordt benadrukt dat het karakter van deze proof of concept het beproeven van de inzichten van Uit betrouwbare bron betreft. Genomen ontwerpkeuzes ten aanzien van het WOZ-domein zijn dus zeker niet in beton gegoten maar kunnen wel inzicht geven in een moeglijke ontwerp-richting voor als het WOZ-register ooit herontworpen word. 

## Objecttype binnen scope

De volgende objecttypen uit het IMWOZ zijn binnen scope voor de proof of concept.

### WOZ-domein :

- WOZObject
- WOZDeelobject
- WOZPRS
- WOZWDO
- Controle objectkenmerken
- WOZCTL
- WOZKOZ
- WOZPRSVES
- SluimerendWOZObject
- SWOKOZ
- Waarde
- Waterschap 

### Personen-domein

- Persoon
- Vestiging

### Kadastraal domein

- Kadastraal Onroerende Zaak

## Aanpassingen per object/relatie

### WOZPRS en WOZPRSVES (objecttype Belang)

De relatieklasse `WOZPRS` in het IMWOZ die op de relatie `heeftBelanghebbende` is gedefinieerd én de relatieklasse WOZPRSVES die op de relatie communiceertVia is gedefinieerd zijn vervangen door het objecttype `Belang`.
In de belanghebbende zijn de verwijzende eigenschappen naar Partij (2 keer) en WOZObject opgenomen. De `Belang` heeft ook een technisch uniek identificerende eigenschap (id) gekregen.

Voor nadere uitleg over keuzes binnen het objecttype `Belang` zie [Documentatie > Primaire view > WOZ-domein > Belang](../documentatie/2821)

### Persoon en Vestiging

De Objecttypen `Persoon` en `Vestiging` zijn vervangen door het Partij-model zoals dat in het traject Klantinteracties is uitgedacht. Hiermee kunnen WOZ-relevante persoons- en organisatiegegevens worden vastgelegd met een verwijzing naar de bron-registratie via de `Partij-identificator`.

    - TODO: Historie en annotatie-asspecten van het Partij-model moeten nog besproken worden.

### WOZObject

- Aan het objecttype `WOZObject` is een technisch uniek identificerende eigenschap (id) toegevoegd. 
- De gegevensgroep aanduidingWOZobject bevat zowel de nummeraanduidingIdentificatie als een aantal adresgegevens die in de BAG aan die nummeraanduiding gerelateerd zijn. Deze gegevensgroep is gereduceerd tot de eigenschap nummeraanduidingIdentificatieWozObject. 
 
    - Vraag: Is er naar aanleidimg van het reduceren van de gegevensgroep aanduidingWozObject behoefte aan een cache van BAG-objecten (Of een Cache van adresgegevens op basis van de BAG)
    - TODO: Historie en annotatie-aspecten van het `WOZObject` moeten nog besproken worden.
    - Vraag: De attributen `gebruikscode` en `functieWOZObject` lijken dezelfde betekenis te hebben. Wat is exact het 
        onderscheid tussen deze twee attributen?

### WOZDeelobject en WOZWDO

Aan het objecttype `WOZDeelobject` is een technisch uniek identificerende eigenschap (id) toegevoegd. 
Aan het objecttype `WOZDeelobject` is een verwijzende eigenschap naar het WOZOject opgenomen.

De relatieklasse `WOZWDO` is verwijderd. De historie die op deze relatie is gedefinieerd zal geïmplementeerd worden op de verwijzende eigenschap in het `WOZDeelobject` naar het `WOZObject`.

    - TODO: Historie en annotatie-aspecten van het `WOZDeelobject` moeten nog besproken worden.

### Waarde en Waterschap

Aan het objecttype `Waarde` is een technisch uniek identificerende eigenschap (id) toegevoegd. 
Aan het objecttype `Waarde` is een verwijzende eigenschap naar het `WOZObject` opgenomen.

    - TODO: Historie en annotatie-aspecten van de `Waarde` moeten nog besproken worden.

Het is maar de vraag of `Waterschap` voor de proof of concept relevant is en binnen scope valt. Totdat daar een definitieve beslissing over genomen is worden er geen ontwerpbeslissingen genomen over `Waterschap` en de relatie tussen `Waterschap` en `Waarde`.

    - TODO: Besluiten of `Waterschap` binnen scope valt. (Is er een casus waarin `Waterschap` een rol speelt.)

### SluimerendWOZObject en SWOKOZ

Het is maar de vraag of `SluimerendWOZObject` en `SWOKOZ` voor de proof of concept relevant zijn en binnen scope vallen. Totdat daar een definitieve beslissing over genomen is worden er geen ontwerpbeslissingen genomen over `SluimerendWOZObject`, `SWOKOZ` en de relatie naar de `KadastraalOnroerendeZaak. terschap` en `Waarde`.

### WOZKOZ

Aan het objecttype `WOZKOZ` is een technisch uniek identificerende eigenschap (id) toegevoegd. 
Aan het objecttype `WOZKOZ` is een verwijzende eigenschap naar het `WOZObject` opgenomen.

    - TODO: Historie en annotatie-aspecten van de `Waarde` moeten nog besproken worden.

### ControleObjectkenmerken

Het objecttype `ControleObjectkenmerken` heeft nu een attribuut `Identificatie` dat een unieke sleutel is voor dit objecttype. Uit consistentie-overwegingen wordt dit attribuut omgenoemd naar `id` en het datatype daarvan wordt een `identificatiecode`.

    - TODO: Historie en annotatie-aspecten van de `ControleObjectkenmerken` moeten nog besproken worden.
    - Vraag: Hier is een meervoudsvorm gekozen voor een objecttype-naam. Dat is byzondr. Ik zou verwachten 
        dat dit objecttype één `controleObjectkenmerk` betreft. 

### WOZCTL

De relatie tussen `WOZObject` en `ControleObjectkenmerken` is een N - M relatie. Om deze te implementeren in een relationeel model gaan we een "koppel-objecttype" introduceren. Vooralsnog noemen we dat objecttype `WOZCTL` tot we een betere naam bedacht hebben.
Verwijzende eigenschap naar het `WOZObject` en naar de `ControleObjectkenmerken` opgenomen. 


     - TODO: Historie en annotatie-aspecten van de `WOZCTL` moeten nog besproken worden.
    













