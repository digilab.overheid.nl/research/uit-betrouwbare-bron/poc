---
title: Semantisch informatiemodel
description: Informatiemodel van de Proof of concept voor de WOZ
weight: 2
draft: false
layout: single
---


## Modellering in Enterprise Architect
Binnen VNG Realisatie wordt Enterprise Architect (EA) gebruikt voor de vastlegging van semantische informatiemodellen (SIM). Onderstaand figuur geeft de modellering van het SIM weer zoals deze in EA is gemodelleerd.
Dit is de 1e versie van het Informatiemodel dat is gepubliceerd. 

<!-- De vorige versies, inclusief een opsomming van de wijzigingen is [hier](./semantisch_model_oud.md) te vinden. -->


<a href="../POCWOZ_overzicht_V001.png" target="_blank">
    <img src="../POCWOZ_overzicht_V001.png" width 1000 alt="Klik hier om groot te openen in een nieuw tabblad.">
</a>