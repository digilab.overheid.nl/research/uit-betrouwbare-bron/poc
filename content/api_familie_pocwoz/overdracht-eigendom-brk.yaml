openapi: 3.0.0
servers:
- description: "SwaggerHub API Auto Mocking"
  url: "https://virtserver.swaggerhub.com/pocwoz/api/correctie/v00"
info:
  title: "Commando-API om comaando's in BRK-context aan te bieden voor omzetting naar de WOZ-context."
  description: "Een API om de eigendoms-overdracht van een Kadastraal Onroerende Zaak die in de BRK-context is geformuleerd om te zetten naar comand's in de WOZ-context."
  version: 0.0.1-oas3.1
  contact:
    url: "https://poc-digilab-overheid-nl-research-uit-betrouwbare-dda02f873817b1.gitlab.io/"
    email: "asterix@uitbetrouwbarebron.nl"
  license:
    name: "European Union Public License, version 1.2 (EUPL-1.2)"
    url: "https://eupl.eu/1.2/nl/"
paths:
  /commando-overdracht-eigendom-BRK:
    post:
      operationId: "overdracht-eigendom-BRK"
      description: "De operatie waarmee een commando in BRK-context aangeboden wordt aan de commandoverwerker om om te zetten naar de WOZ-context."
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Commando"
      responses:
        201:
          description: "Command delivered"
          headers:
#            Location:
#              description: "URI van de nieuwe resource"
#              schema:
#                type: "string"
#                format: "uri"
#                example: "/soortendigitaaladres/"
            api-version:
              $ref: "https://raw.githubusercontent.com/VNG-Realisatie/API-Kennisbank/master/common/common.yaml#/components/headers/api_version"
            warning:
              $ref: "https://raw.githubusercontent.com/VNG-Realisatie/API-Kennisbank/master/common/common.yaml#/components/headers/warning"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Commando"
        400:
          $ref: "https://raw.githubusercontent.com/VNG-Realisatie/API-Kennisbank/master/common/common.yaml#/components/responses/400"
        401:
          $ref: "https://raw.githubusercontent.com/VNG-Realisatie/API-Kennisbank/master/common/common.yaml#/components/responses/401"
        403:
          $ref: "https://raw.githubusercontent.com/VNG-Realisatie/API-Kennisbank/master/common/common.yaml#/components/responses/403"
        415:
          $ref: "https://raw.githubusercontent.com/VNG-Realisatie/API-Kennisbank/master/common/common.yaml#/components/responses/415"
        500:
          $ref: "https://raw.githubusercontent.com/VNG-Realisatie/API-Kennisbank/master/common/common.yaml#/components/responses/500"
        503:
          $ref: "https://raw.githubusercontent.com/VNG-Realisatie/API-Kennisbank/master/common/common.yaml#/components/responses/503"
        default:
          $ref: "https://raw.githubusercontent.com/VNG-Realisatie/API-Kennisbank/master/common/common.yaml#/components/responses/default"
      tags:
      - "Eigendomsoverdracht BRK-ontext"
components:
  schemas:
    Commando:
      type: "object"
      description: "Commando dat aan een commando-verwerker wordt aangeboden om omgezet te worden van de BRK-context naar de WOZ-context."
      required:
        - type
        - fields
      discriminator:
        propertyName: commandoType
        mapping:
          VolledigeEigendomsoverdracht: '#/components/schemas/VolledigeEigendomsoverdrachtBrk'
      properties:
        commandoType: 
          $ref: "#/components/schemas/Commandotype" 
    VolledigeEigendomsoverdrachtBrk:
      type: "object"
      description: "Benodigde gegevens voor het vastleggen van een eigendomsoverdracht in de WOZ-registratie in de BRK-context geformuleerd. "
      required:
      - "wozObjectnummer"
      - "eigenaren"
      - "geldigVanaf"
      allOf:
        - $ref: '#/components/schemas/Commando'
        - type: object
      properties:
        aardStukdeel:
          $ref: "#/components/schemas/AardStukdeel"
        ingeschrevenStuk:
          $ref: "#/components/schemas/IngeschrevenStuk"
        kadastraalObject: 
          $ref: "#/components/schemas/KadastraalObject"
        geldigVanaf: 
          $ref: "#/components/schemas/DatumTijd"
    AardStukdeel:
      type: "object"
      properties:
        code: 
          $ref: "#/components/schemas/Code"
        waarde:
          $ref: "#/components/schemas/Waarde"
    Aandeel:
      type: "number"
      format: float
      description: "Procentueel aandeel in de eigendom van een WOZObject"
      maximum: 100
    Burgerservicenummer:
      type: "string"
      description: "Identificatie zoals die door de Basisregistratie Personen wordt gehanteerd."
      maxLength: 9
    Code:
      type: "string"
      maxLength: 10
    Commandotype:
      type: "string"
      description: "Aanduiding van datum volgens de NEN-ISO 8601:2019-standaard. Een datum wordt genoteerd van het meest naar het minst significante onderdeel. Een voorbeeld: 2022-02-21 representeert 21 februari 2022."
      format: "date"
    Datum:
      type: "string"
      description: "Datum"
    DatumTijd:
      type: "string"
      description: Datum en tijdnotatie conform de NEN-ISO 8601:2019 standaard"
    Eigenaar:
      type: "object"
      properties:
        soortPartij:
          $ref: "#/components/schemas/SoortPartij"
        burgerservicenummer: 
          $ref: "#/components/schemas/Burgerservicenummer"
        aandeel:
          $ref: "#/components/schemas/Aandeel"
    Identificatiecode:
      type: "string"
      description: "Unieke technische identificerende code, primair bedoeld voor gebruik bij interacties tussen IT-systemen."
      minLength: 1
      maxLength: 40
    IngeschrevenStuk:
      type: "object"
      properties: 
        tijdstipOndertekening:
          $ref: "#/components/schemas/TijdstipOndertekening"
    KadastraalObject:
      type: "object"
      properties:
        kadastraleGemeente:
          $ref: "#/components/schemas/KadastraleGemeente"
        sectie:
          $ref: "#/components/schemas/Sectie"
        perceelnummer:
          $ref: "#/components/schemas/Perceelnummer"
    KadastraleGemeente:
      type: "object"
      properties: 
        code:
          $ref: "#/components/schemas/Code"
        waarde:
          $ref: "#/components/schemas/Waarde"
    Naam-kort:
      type: "string"
      description: "Kort (80 karakters) tekstveld voor een naam."
      maxLength: 80
    Perceelnummer:
      type: "string"
    Sectie:
      type: "string"
    SoortPartij: 
      type: "string"
      enum:
      - "Persoon"
      - "Organisatie"
    Tijd:
      type: "string"
    TijdstipOndertekening:
      type: "object"
      properties:
        datum: 
          $ref: "#/components/schemas/Datum"
        tijd:
          $ref: "#/components/schemas/Tijd"
    Waarde: 
      type: "string"
      maxLength: 100
      