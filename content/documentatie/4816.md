---
title: Initiële vulling partij
category: pocwoz
doc-type: casus
navigation: Casussen > Partij-domein
preliminary: true
draft: false
layout: single
weight: 2
---

# Initële registratie van personen t.b.v. diverse casussen

## Inleiding

Er worden conform de onderstaande beschrijving drie personen opgevoerd met de [F2976 - Registreer partij](./2976)

## Persoon 1

### Partij

| ***`Objecttype`*** - Attribuut | Waarde | Toelichting |
| :----------- | :----------- | :----------- |
| **`Partij`** | | |
| - Id | `<Partij` wordt geïdentificeerd door `<67cb34d0-8149-405a-968c-ad045f2ff73e>` | |
| - Nummer | `<Partij>` wordt ook geïdentificeerd met `<123454321>` | |
| - Bezoekadres | | |
|&nbsp;&nbsp; - NummeraanduidingID | `<Partij>` kan bezocht worden op adres met `<0518200000839598>` | | 
|&nbsp;&nbsp; - Adresregel 1 | Nassaulaan 2 A |
|&nbsp;&nbsp; - Adresregel 2 | 2514 JS  'S-GRAVENHAGE | |
|&nbsp;&nbsp; - Adresregel 3 | | |
|&nbsp;&nbsp; - Land | 6030 | Verwijzing naar referentielijst `Land` |
| - Correspondentieadres | | |
|&nbsp;&nbsp; - NummeraanduidingID | 0518200000839598 | | 
|&nbsp;&nbsp; - Adresregel 1 | Nassaulaan 2 A |
|&nbsp;&nbsp; - Adresregel 2 | 2514 JS  'S-GRAVENHAGE | |
|&nbsp;&nbsp; - Adresregel 3 | | |
|&nbsp;&nbsp; - Land | 6030 | Verwijzing naar referentielijst `Land` |
| - Interne Notitie  | | |
| - Soort partij | persoon | |
| - Indicatie geheimhouding | false | |
| - Voorkeurstaal | 001 | Hier wordt technisch een code gebruikt die in de referentielijst `Taal` naar de hier getoonde waarde verwijst |
| - Indicatie actief | true | | 
| - Voorkeurs digitaal adres | | |
| - Contactnaam | | |
|&nbsp;&nbsp; - Voorletters | JS | |
|&nbsp;&nbsp; - Voornaam | Johan | | 
|&nbsp;&nbsp; - Voorvoegsel achternaam | | |
|&nbsp;&nbsp; - Achternaam | Bach | | 


### Partij-identificator

| ***`Objecttype`*** - Attribuut | Waarde | Toelichting |
| :----------- | :----------- | :----------- |
| `Partijdidentificator` | | |
| - id | c2bf75dd-1bc1-42d9-9473-51a94a408d7c | |
| - anderePartijIdentificator | | | 
| - identificeerdePartij | 67cb34d0-8149-405a-968c-ad045f2ff73e | | 
| - partijidentificator | | |  
|&nbsp;&nbsp; - Object ID | 999990019 | |
|&nbsp;&nbsp; - Soort object | 'Ingeschreven Persoon' | | 
|&nbsp;&nbsp; - Soort object ID | 'Burgerservicenummer' | Hier wordt technisch een code gebruikt die in de referentielijst soortObjectId naar de hier getoonde waarde verwijst |
|&nbsp;&nbsp; - Register | 'Basisregistratie Personen' | Hier wordt technisch een code gebruikt die in de referentielijst externRegsister naar de hier getoonde waarde verwijst |

