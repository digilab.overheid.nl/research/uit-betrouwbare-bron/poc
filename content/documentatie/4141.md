---
title: Vul referentielijsten Partij-domein
category: pocwoz
doc-type: casus
navigation: Casussen > Partij-domein
preliminary: true
draft: false
layout: single
weight: 1
---



## Inleiding 

In deze casus worden alleen de waarden beschreven van de referentielijsten die nodig zijn om de andere casussen te kunnen uitvoeren. Er volgen dus geen compleet gevulde referentielijsten uit, maar alleen de waarden in de referentielijsten die nodig zijn. 

We conformeren ons aan bestaande definities van de referentielijsten met als uitzondering dat historie-aspecten (ingangsdatumLand, einddatumLand en indicatieActief). Zie voor onderbouwing vraag [V4757 - Historie in referentielijsten](../4757.md). 
De referentielijsten voor het Partij-domein zijn ontleend aan het informatiemodel voor Klantinteracties. 

Bij het toewijzen van Claim-id's wordt er in de praktijk gebruik gemaakt van UUID's. De Claimid's die in de onderstaande tabellen gebruikt worden hebben als doel om de samenhang tussen claims onderling en claims en annotaties weer te geven. 

## Referentielijst Land

### Tabelweergave inhoud Land 

| **landId** | **landcode** | **landnaam** |
| :---- | :----- | :----- |
| 1e67d373-e2f5-4b78-8dac-954d42758274 | 6030 | Nederland | 

#### Claims voor Land met landcode "003"

Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| L001 | Land | Er bestaat een land geïdentificeerd door `<1e67d373-e2f5-4b78-8dac-954d42758274>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN001 | Geregistreerd op (A3) | `<L001>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN002 | Aanvang geldigheid (A4) | De geldigheid van `<L001>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| L002 | Land <landId> / landcode| `Land <<1e67d373-e2f5-4b78-8dac-954d42758274>`wordt ook aangeduid met code `<003>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- |
| AN023 | Geregistreerd op (A3) | `<L002>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN024 | Aanvang geldigheid (A4) | De geldigheid van `<L002>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| L002 | Land <landId> / landcode| `Land <<1e67d373-e2f5-4b78-8dac-954d42758274>` heeft als naam `<Nederland>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- |
| AN023 | Geregistreerd op (A3) | `<L003>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN024 | Aanvang geldigheid (A4) | De geldigheid van `<L003>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


### Referentielijst Taal

#### Tabelweergave inhoud Taal

| **code** | **naam** | 
| :---- | :----- | 
| 17 | Nederlands | 

#### Claims voor Land met code "17"

Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| T001 | Taal | Er bestaat een taal geïdentificeerd door `<17>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN026 | Geregistreerd op (A3) | `<T001>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN027 | Aanvang geldigheid (A4) | De geldigheid van `<T001>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| T002 | Taal <code> / naam | `Taal <17>` heeft als naam `<Nederlands>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN029 | Geregistreerd op (A3) | `<T002>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN030 | Aanvang geldigheid (A4) | De geldigheid van `<T002>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


### Referentielijst Soort object

#### Tabelweergave inhoud Soort object

| **code** | **naam** | 
| :---- | :----- | 
| 1 | Ingeschreven persoon | 
| 2 | Kadastraal onroerende zaak |
| 3 | Vestiging | 

#### Claims voor Soort object met code "1"

Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| SO001 | SoortObject | Er bestaat een soort object geïdentificeerd door `<1>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN032 | Geregistreerd op (A3) | `<SO001>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN033 | Aanvang geldigheid (A4) | De geldigheid van `<SO001>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| SO002 | SoortObject <code> / naam | `SoortObject <1>` heeft als naam `<Ingeschreven persoon>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN035 | Geregistreerd op (A3) | `<SO002>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN036 | Aanvang geldigheid (A4) | De geldigheid van `<SO002>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


#### Claims voor Soort object met code "2"

Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| SO003 | SoortObject | Er bestaat een soort object geïdentificeerd door `<2>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- |
| AN038 | Geregistreerd op (A3) | `<SO003>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN039 | Aanvang geldigheid (A4) | De geldigheid van `<SO003>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| SO004 | SoortObject <code> / naam | `SoortObject <2>` heeft als naam `<Kadastraal onroerende zaak>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN041 | Geregistreerd op (A3) | `<SO004>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN042 | Aanvang geldigheid (A4) | De geldigheid van `<SO004>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


#### Claims voor Soort object met code "3"

Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| SO005 | SoortObject | Er bestaat een soort object geïdentificeerd door `<3>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- |
| AN044 | Geregistreerd op (A3) | `<SO005>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN045 | Aanvang geldigheid (A4) | De geldigheid van `<SO005>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| SO006 | SoortObject <code> / naam | `SoortObject <3>` heeft als naam `<Vestiging>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN047 | Geregistreerd op (A3) | `<SO006>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN048 | Aanvang geldigheid (A4) | De geldigheid van `<SO006>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


### Referentielijst Soort object id

#### Tabelweergave inhoud Soort object id

| **code** | **naam** | 
| :---- | :----- | 
| 1 | Burgerservicenummer | 
| 2 | RSIN | 
| 3 | Kadastrale identificatie |

#### Claims voor Soort object id met code "1"

Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| SOI001 | SoortObjectId | Er bestaat een soort object id geïdentificeerd door `<1>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN050 | Geregistreerd op (A3) | `<SOI001>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN051 | Aanvang geldigheid (A4) | De geldigheid van `<SOI001>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| SOI002 | SoortObjectId <code> / naam | `SoortObjectId <1>` heeft als naam `<Burgerservicenummer>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN053 | Geregistreerd op (A3) | `<SOI002>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN054 | Aanvang geldigheid (A4) | De geldigheid van `<SOI002>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


#### Claims voor Soort object id met code "2"

Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| SOI003 | SoortObjectId | Er bestaat een soort object id geïdentificeerd door `<2>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN056 | Geregistreerd op (A3) | `<SOI003>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN057 | Aanvang geldigheid (A4) | De geldigheid van `<SOI003>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| SOI004 | SoortObjectId <code> / naam | `SoortObjectId <2>` heeft als naam `<RSIN>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN059 | Geregistreerd op (A3) | `<SOI004>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN060 | Aanvang geldigheid (A4) | De geldigheid van `<SOI004>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


#### Claims voor Soort object id met code "3"

Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| SOI005 | SoortObjectId | Er bestaat een soort object id geïdentificeerd door `<3>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN062 | Geregistreerd op (A3) | `<SOI005>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN063 | Aanvang geldigheid (A4) | De geldigheid van `<SOI005>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| SOI006 | SoortObjectId <code> / naam | `SoortObjectId <3>` heeft als naam `<Kadstrale identificatie>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN065 | Geregistreerd op (A3) | `<SOI006>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN066 | Aanvang geldigheid (A4) | De geldigheid van `<SOI006>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |



### Referentielijst Extern register

#### Tabelweergave inhoud Extern register

| **code** | **naam** | **locatie** | 
| :---- | :----- | :----- | 
| 1 | Basisregistratie Personen | https://www.RVIG.nl/locatie | 
| 2 | Basisregistratie Kadaster | https://www.Kadaster.nl/locatie | 

#### Claims voor Extern register met code "1"

Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| ER001 | ExternRegister | Er bestaat een extern register geïdentificeerd door `<1>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN068 | Geregistreerd op (A3) | `<ER001>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN069 | Aanvang geldigheid (A4) | De geldigheid van `<ER001>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| ER002 | ExternRegister <code> / naam | `ExternRegister <1>` heeft als naam `<Basisregistratie Personen>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN071 | Geregistreerd op (A3) | `<ER002>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN072 | Aanvang geldigheid (A4) | De geldigheid van `<ER002>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| ER003 | ExternRegister <code> / locatie | `ExternRegister <1>` heeft als locatie `<https://www.RVIG.nl/locatie>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN074 | Geregistreerd op (A3) | `<ER003>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN075 | Aanvang geldigheid (A4) | De geldigheid van `<ER003>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


#### Claims voor Extern register met code "3"

Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| ER004 | ExternRegister | Er bestaat een extern register geïdentificeerd door `<2>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN077 | Geregistreerd op (A3) | `<ER004>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN078 | Aanvang geldigheid (A4) | De geldigheid van `<ER004>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| ER005 | ExternRegister <code> / naam | `ExternRegister <2>` heeft als naam `<Basisregistratie Kadaster>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN080 | Geregistreerd op (A3) | `<ER005>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN081 | Aanvang geldigheid (A4) | De geldigheid van `<ER005>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| ER006 | ExternRegister <code> / locatie | `ExternRegister <2>` heeft als locatie `<https://www.kadaster.nl/locatie>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN083 | Geregistreerd op (A3) | `<ER006>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN084 | Aanvang geldigheid (A4) | De geldigheid van `<ER006>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |



### Referentielijst Soort digitaal adres

#### Tabelweergave inhoud Extern register

| **code** | **naam** |
| :---- | :----- | 
| 1 | email | 
| 2 | telefoon | 


#### Claims voor Soort digitaal adres met code "1"

Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| SDA001 | SoortDigitaalAdres | Er bestaat een soort digitaal adres geïdentificeerd door `<1>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN086 | Geregistreerd op (A3) | `<SDA001>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN087 | Aanvang geldigheid (A4) | De geldigheid van `<SDA001>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |

Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| SDA002 | SoortDigitaalAdres <code> / naam | `SoortDigitaalAdres <1>` heeft als naam `<email>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN089 | Geregistreerd op (A3) | `<SDA002>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN090 | Aanvang geldigheid (A4) | De geldigheid van `<SDA002>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


#### Claims voor Soort digitaal adres met code "2"

Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| SDA003 | SoortDigitaalAdres | Er bestaat een soort digitaal adres geïdentificeerd door `<2>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- |
| AN092 | Geregistreerd op (A3) | `<SDA003>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN093 | Aanvang geldigheid (A4) | De geldigheid van `<SDA003>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |


Claimid | Claimtype | Claim-expression | 
| :----------- | :----------- | :----------- |
| SDA004 | SoortDigitaalAdres <code> / naam | `SoortDigitaalAdres <2>` heeft als naam `<telefoon>` |

Claimid | Annotatietype | AnnotatieClaim-expression | 
| :----------- | :----------- | :----------- | 
| AN095 | Geregistreerd op (A3) | `<SDA004>` is geregistreerd op `<2024-01-01T12:00:00.123+00:00>` |
| AN096 | Aanvang geldigheid (A4) | De geldigheid van `<SDA004>` is aangevangen op `<2024-01-01T12:00:00.123+00:00>` |

