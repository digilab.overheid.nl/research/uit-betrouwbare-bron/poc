---
title: Annotatie-claimtypes
category: pocwoz
doc-type: 
navigation: Primaire view > Generiek
preliminary: true
draft: false
layout: single
weight: 1
---

## Inleiding

We onderkennen een aantal annotatie-typen op basis van de semantiek uit het InformatieModel WOZ. Tijdens het uitwerken van de casussen gaan we ontdekken welke annotatietype op welke claimtypen van toepassing kunnen zijn. 
Op voorhand worden hier geen beslissingen over genomen en vooralsnog stellen we dat de engine geen beperkingen oplegt en dat de keuze welke annotatietypen van toepassing zijn op welke claimtypen een domein-keuze is. 


## Annotatie-typen

- Context (A1)
- Aanvang onderzoek (A2)
- Geregistreerd op (A3)
- Vervallen op (A4)
- Geldig vanaf (A5)
- Geldig tot (A6)
- ~~Geregistreerd door (A7)~~
- Einde onderzoek (A7)

### Annotatietype Context (A1)

Dit annotatietype is geïdentificeerd naar aanleiding van de behoefte om te registreren welke claim heeft geleid tot het aanmaken van een claim. Hiermee kan van een claim wordt achterhaald welke notificatie, commando of effect heeft geleid tot het vastleggen van deze claim.  

| ***`Annotatietype`*** - eigenschap | Claimtype - expression | Annotatietypen | 
| :----------- | :----------- | :----------- | :----------- | 
| ***`Context`*** | `<$Claimid>` is geregistreerd naar aanleiding van `<$Claimid>`| A3 | 

### Annotatietype Aanvang Onderzoek (A2)

Dit annotatietype is geïdentificeerd naar aanleiding van de gegevensgroep `inOnderzoek` die onderdeel is van diverse objecttypen in het IMWOZ. 

Per objecttype waarbij onderzoek relevant kan zijn is een enumeratie opgenomen van waarden die kunnen voorkomen. Deze enumeratiewaarden komen overeen met de attributen (eigenschappen) die bij een object in onderzoek gezet kunnen worden. Over het karakter van het onderzoek of de mate van twijfel wordt niet gesproken in het IMWOZ. De functionaliteit is in het claimdenken te bereiken door een annotie van het type `Onderzoek` op een claim te plaatsen.   

| ***`Annotatietype`*** - eigenschap | Claimtype - expression | Annotatietypen | 
| :----------- | :----------- | :----------- | :----------- | 
| ***`Aanvang Onderzoek`*** | Er is een onderzoek gestart naar `<$claim>`| A1, A3, A4 |  

### Annotatietype Geregistreerd op (A3)

Dit annotatietype is geïdentificeerd naar aanleiding de behoefte om binnen een capabel register vast te kunnen leggen wanneer een claim is geregistreerd. 

| ***`Annotatietype`*** - eigenschap | Claimtype - expression | Annotatietypen | 
| :----------- | :----------- | :----------- | :----------- | 
| ***`Geregistreerd op`*** | `<$claim>` is geregistreerd op `<datumTijdstip>` | |  

### Annotatietype Vervallen op (A4)

Dit annotatietype is geïdentificeerd naar aanleiding de behoefte om binnen een capabel register vast te kunnen leggen wanneer een claim is vervallen. 

| ***`Annotatietype`*** - eigenschap | Claimtype - expression | Annotatietypen | 
| :----------- | :----------- | :----------- | :----------- | 
| ***`Vervallen op`*** | `<$claim>` is vervallen op `<datumTijdstip>` | A1, A3 |  

    - Graag bespreken of er op een `Vervallen op` een geldigheidstijdlijn va toepassing is. 

### Annotatietype Aanvang geldigheid (A5)

Dit annotatietype is geïdentificeerd naar aanleiding de behoefte om binnen een capabel register vast te kunnen leggen vanaf wanneer de geldigheid van een claim aanvangt.  

| ***`Annotatietype`*** - eigenschap | Claimtype - expression | Annotatietypen | 
| :----------- | :----------- | :----------- | :----------- | 
| ***`Geldig vanaf`*** | De geldigheid van `<$claim>` is aangevangen op `<datumTijdstip>` | A1, A3, A4 |  

### Annotatietype Einde geldigheid (A6)

Dit annotatietype is geïdentificeerd naar aanleiding de behoefte om binnen een capabel register vast te kunnen leggen vanaf wanneer de geldigheid van een claim is geëindigd. 

| ***`Annotatietype`*** - eigenschap | Claimtype - expression | Annotatietypen | 
| :----------- | :----------- | :----------- | :----------- | 
| ***`Geldig tot`*** | De geldigheid van `<$claim>` is geeindigd op `<datumTijdstip>` | A1, A3, A4 |  

### ~~Annotatietype Geregistreerd door (A7)~~

~~Dit annotatietype is geïdentificeerd om vast te kunnen leggen welke actor (medewerker / applicatiecomponent) vastlegging heeft uitgevoerd.~~

~~We kiezen er nu voor om in de `Geregistreerd door` annotatie te verwijzen naar een object [Actor](./5806).~~

~~| ***`Annotatietype`*** - eigenschap | Claimtype - expression | Annotatietypen | ~~
~~| :----------- | :----------- | :----------- | :----------- | ~~
~~| **`Geregistreerd door`** | `<$claim>` is geregistreerd door `<Actor>` | |  ~~

### Annotatietype Einde Onderzoek (A7)

Bij claims waarbij de aanvang van een onderzoek is geregistreerd met het annotatietype `Aanvang onderzoek`, moet het mogelijk zijn om het beeindigen van dat onderzoek te registreren. Daarvoor hebben we het annotatietype `Einde Onderzoek`. 

| ***`Annotatietype`*** - eigenschap | Claimtype - expression | Annotatietypen | 
| :----------- | :----------- | :----------- | :----------- | 
| ***`Einde Onderzoek`*** | Het onderzoek naar `<$claim>` is beeindigd | A1, A3, A4 |  