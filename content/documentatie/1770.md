---
title: Referentielijsten Partij-domein
category: pocwoz
doc-type: 
navigation: Primaire view > Partij-domein
preliminary: true
draft: false
layout: single
weight: 1
---



## Inleiding 

We conformeren ons aan bestaande definities van de referentielijsten met als uitzondering dat historie-aspecten (ingangsdatumLand, einddatumLand en indicatieActief). Zie voor onderbouwing vraag [V4757 - Historie in referentielijsten](../4757.md). 

De referentielijsten voor het Partij-domein zijn ontleend aan het informatiemodel voor Klantinteracties. 


## Land

Lijst van alle huidige en voormalige landen met hun codes, namen en geldigheidsinformatie.
De referentielijst Land is gebaseerd op de landelijke tabel 34 Landen

### Referentielijst

| ***`Objecttype`*** - eigenschap | Claimtype-Expression / Objecttype-expression | Annotatietypen |
| :----------- | :----------- | :----------- | 
| ***`Land`*** | Er bestaat een land geïdentificeerd door `<landId>` /  `Land <landId>` | A3, A4, A5, A6  |
| - landId | *Zie land* |  |
| - landcode | `<Land>` wordt ook aangeduid met code `<landcode>` | A3, A4, A5, A6  |
| - landnaam | `<Land>` heeft als naam `<landnaam>` | A3, A4, A5, A6  |

## Taal

Lijst van talen die personen en organisaties kunnen gebruiken bij contact met de gemeente en vice versa.

### Referentielijst

| ***`Objecttype`*** - eigenschap | Claimtype-Expression / Objecttype-expression | Annotatietypen |
| :----------- | :----------- | :----------- | 
| **`Taal`** | Er bestaat een  taal geïdentificeerd door `<code>` / `Taal <code>` | A3, A4, A5, A6  |
| - code | *zie Taal* | |
| - naam | `<Taal>` heeft als naam `<naam>`| A3, A4, A5, A6  |

## Soort object

Lijst van typen objecten waarnaar vanuit het klantinteractiesregister relaties kunnen worden gelegd.

### Referentielijst

| ***`Objecttype`*** - eigenschap | Claimtype-Expression / Objecttype-expression | Annotatietypen |
| :----------- | :----------- | :----------- | 
| **`SoortObject`** | Er bestaat een SoortObject geïdentificeerd door `<code>` / `SoortObject <code>` | A3, A4, A5, A6  |
| - code | *zie soortObjecttype* |  |
| - naam | `<SoortObject>` heeft als naam `<naam>`| A3, A4, A5, A6  |

## Soort objectID

Lijst van identificatiesystemen en de registers waarin volgens deze systemen te identificeren objecten gevonden kunnen worden.

### Referentielijst

| ***`Objecttype`*** - eigenschap| Claimtype-Expression / Objecttype-expression | Annotatietypen |
| :----------- | :----------- | :----------- | 
| ***`SoortObjectId`*** | Er bestaat een SoortObjectId geïdentificeerd door `<code>` / `SoortObjectId <code>` | A3, A4, A5, A6  |
| - code | *zie SoortObjectId* | |
| - naam | `<SoortObjectId>` heeft als naam `<naam>`| A3, A4, A5, A6  |

## Extern register

Lijst van registers buiten het domein van de WOZ waarin objecten zijn geregistreerd die binnen het domein van WOZ een rol (kunnen) spelen.

### Referentielijst

| ***`Objecttype`*** - eigenschap| Claimtype-Expression / Objecttype-expression | Annotatietypen |
| :----------- | :----------- | :----------- | 
| **`ExternRegister`** | Er bestaat een ExternRegister geïdentificeerd door `<code>` / `ExternRegister <code>` | A3, A4, A5, A6  |
| - code | *zie ExternRegister* |  |
| - naam | `<ExternRegister>` heeft als naam `<naam>`| A3, A4, A5, A6  |
| - locatie | `<ExternRegister>` is bereikbaar op `<locatie>` | A3, A4, A5, A6  |

## Soort digitaal adres

Lijst van soorten digitale adressen die personen en organisaties kunnen gebruiken voor contact met de gemeente en vice versa.

### Referentielijst

| ***`Objecttype`*** - eigenschap | Claimtype-Expression / Objecttype-expression | Annotatietypen |
| :----------- | :----------- | :----------- |
| **`SoortDigitaalAdres`** | Er bestaat een SoortDigitaalAdres geïdentificeerd door `<code>` / `SoortDigitaalAdres <code>` | A3, A4, A5, A6  |
| code | *zie SoortDigitaalAdres* | |
| naam | `<SoortDigitaalAdres>` heeft als naam `<naam>`| A3, A4, A5, A6  |
