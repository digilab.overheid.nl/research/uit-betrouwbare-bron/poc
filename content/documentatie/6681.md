---
title: Objecttype WOZObject
category: pocwoz
doc-type: 
navigation: Primaire view > WOZ-domein
preliminary: true
draft: false
layout: single
weight: 2
---

## Context

Beschrijving van de primaire view op het objecttype `WOZObject` middels claimtype-expressies.  

Toelichting op het object: De onroerende zaak waarvan op grond van de Wet WOZ de waarde moet worden bepaald en vastgesteld.

## WOZObject

| ***`Objecttype`*** - eigenschap | Claimtype-expression / Objecttype-expression | Annotatietypen
| :----------- | :----------- | :----------- | 
| **`WOZObject`** | Er bestaat een `WOZObject` geïdentificeerd door `<wozObjectnummer>` / `WOZObject <wozObjectNummer>` | A3, A4, A5, A6, A7 | 
| - wozObjectnummer | *zie WOZObject* | | 
| - statusWozObject | `<WOZObject>` bevindt zich in de levenscyclus-fase `<statusWOZObject>` | A2, A3, A4, A5, A6, A7 |
| - gebruikscode | `<WOZObject>` wordt gebruikt als `<gebruikscode>` | A2, A3, A4, A5, A6, A7 |
| - typeWOZObject | `<WOZObject>` is getypeerd door `<typeWOZObject>` | A2, A3, A4, A5, A6, A7 |
| - typeWOZObjectOptioneel | `<WOZObject>` is aanvullend getypeerd door `<typeWOZObject>` | A3, A4, A5, A6, A7 |
| - functieWOZObject | `<WOZObject>` heeft als functie `<functieWOZObject>` | A3, A4, A5, A6, A7 | # Wat is dit anders dan gebruikscode? 
| - aanduidingRepeterend | `<WOZObject>` maakt deel uit van een groep direct vergelijkbare objecten `<aanduidingRepeterend>` | A3, A4, A5, A6, A7 |
| - aanduidingInAanbouw	 | `<WOZObject>` is in aanbouw `<aanduidingInAanbouw>` | A3, A4, A5, A6, A7 | 
| - soortObjectCode	 | `<WOZObject>` waarde wordt mede bepaald aan de hand van `<soortObjectCode>` | A3, A4, A5, A6, A7 |
| - codeGebouwdOngebouwd | `<WOZObject>` is volgens de Waterschapswet ten behoeve van de heffing van de waterschapsysteemheffing aangeduid als `<codeGebouwdOngebouwd>` | A2, A3, A4, A5, A6, A7 |
| - wozObjectGeometrie	 | `<WOZObject>` heeft als tweedimensionale geometrische representatie van de omtrek `<wozObjectGeometrie>` | A2, A3, A4, A5, A6, A7 |
| - grondoppervlakte	 | `<WOZObject>` heeft als grondoppervlakte in vierkante meters `<grondoppervlakte>` | A2, A3, A4, A5, A6, A7 |
| - meegetaxeerdeOppervlakte Gebouwd | `<WOZObject>` waardebepaling vindt mede plaats op basis van de betrokken grondoppervlakte in vierkante meters `<meegetaxeerdeOppervlakteGebouwd>` | A2, A3, A4, A5, A6, A7 |
| - monumentaanduiding	 | `<WOZObject>` is volgens de Monumentenwet van 1988 aangeduid als een monument van het type `<monumentaanduiding>` | A3, A4, A5, A6, A7 |
| - aanwezigheidLift	 | `<WOZObject>` kan bereikt worden middels een lift `<aanwezigheidLift>` | A3, A4, A5, A6, A7 |
| - waardegebied | `<WOZObject>` waarde wordt mede bepaald omdat het ligt in gebied `<waardegebied>` | A3, A4, A5, A6, A7 |
| - indicatieLigging | De waarde van `<WOZObject>` wordt mede bepaald door de kwaliteit van de omgeving die wordt aangeduid met `<indicatieLigging>` | A3, A4, A5, A6, A7 |
| - codeOntbrekenNutsvoorzieningen| `<WOZObject>` is niet aangesloten op `<codeOntbrekenNutsvoorzieningen>` | A3, A4, A5, A6, A7 |
| - codeOmzetBelasting | De omzetbelasting wordt op `<codeOmzetBelasting>` verwerkt bij het bepaalen van de waarde van `<WOZObject>` | A3, A4, A5, A6, A7 |
| - ozbVrijstelling	 | De waarde van `<WOZObject>` wordt `<ozbVrijstelling>` gebruikt als heffingsmaatstaf voor de Onroerende Zaak Belasting | A2, A3, A4, A5, A6, A7 | 
| - aanduidingBouwstroom | `<WOZObject>` maakt onderdeel uit van bouwstroom `<aanduidingBouwstroom>` van gelijktijdig en nagenoeg identiek gebouwde WOZObjecten | A3, A4, A5, A6, A7 |
| - groepaanduiding	 | `<WOZObject>` maakt onderdeel uit van de groep `<groepaanduiding>` vergelijkbare objecten waarvoor dezelfde marktgegevens gebruikt kunnen worden bij de waardebepaling en de onderbouwing op het taxatieverslag | A3, A4, A5, A6, A7 |
| - omschrijving | `<WOZObject>` wordt nader omschreven met `<omschrijving>`  | A3, A4, A5, A6, A7 |
| - documentIdentificatie | Zie vraag | | 
| - verantwoordelijkeGemeente	 | `<verantwoordelijkeGemeente>` is verantwoordelijk voor de waardebepaling en de waardevaststelling van `<WOZObject>` | A3, A4, A5, A6, A7 |
| - nummeraanduidingIdentificatie | Op WOZ-beschikking van `<WOZObject>` wordt het adres vermeld dat in de BAG aangeduid wordt met met `<nummeraanduidingIdentificatie>` | 
| - adresregel1Beschikking | `<adresregel1Beschikking>` wordt als eerste adresregel vermeld op de WOZ-beschikking van `<WOZObject>` | A3, A4, A5, A6, A7 | 
| - adresregel2Beschikking | `<adresregel2Beschikking>` wordt als tweede adresregel vermeld op de WOZ-beschikking van `<WOZObject>` | A3, A4, A5, A6, A7 |
| - adresregel3Beschikking | `<adresregel3Beschikking>` wordt als derde adresregel vermeld op de WOZ-beschikking van `<WOZObject>` | A3, A4, A5, A6, A7 |


## Vragen

###  Kardinaliteiten

    - TODO: Kardinaliteiten zijn hier niet vastgelegd. Hoe gaan we aangeven dat een attribuut
        (claimptype) optioneel is of meermaals mag voorkomen (codeOntbrekendeNutsvoorziening) ?

### Documentidentificatie

    - TODO: Hier moet een nette verwijzing naar een document komen. Vraag is of dat document een object binnen het domein is. Hoe gaan we deze verwijzing vormgeven als dat niet zo is. 

### identificatieNummeraanduiding

    - TODO: Gaarne review van de Claimtype expressie voor identificatieNummeraanduiding. Dit is een verwijzing naar een objecttype dat buiten het WOZ-domein bestaat.

## Toelichting van ontwerpkeuzes t.o.v. het IMWOZ. 

- inOnderzoek wordt middels annotaties ingevuld. 
- ingangsdatumObject wordt middels annotaties ingevuld. 
- einddatumObject wordt middels annotaties ingevuld.
- De gegevensgroep aanduidingWOZobject duidt niet op een object met zelfstandig bestaan. 
    - Als een BAG-adres wordt vermeldt op de WOZ-beschikking wordt met identificatieNummeraanduiding verwezen naar het betreffende BAG-adres.
    - Als op de WOZ-beschikking geen BAG-adres kan worden vermeld dan worden de adresgegevens zoals die op de WOZ_beschikking moetn worden opgenomen expliciet opgenomen bij het WOZObject. 
- De attributen voor het adres op de WOZ-beschikking voor een niet-BAG-adres zijn vervangen door drie adresregels. 

