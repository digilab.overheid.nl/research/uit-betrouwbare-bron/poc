---
title: Wat is de Proof of Concept?
description: Wat is de proof of concept
draft: false
layout: single
weight: 1
---

## Inleiding:

Om de inzichten die we in het project Uit betrouwbare bron opdoen te toetsen voeren we parallel aan het opdoen van die inzichten een proof of concept uit die gebaseerd is op een praktijksituatie. Deze proof of concept wordt gedaan op een zeer beperkte selectie van casussen uit het WOZ-domein. De doelstelling van deze proof of concept is het aantonen dat de concepten die door Uit betrouwbare bron zijn beschreven in de praktijk werkbaar zijn. 

## Scope

Het volledige WOZ-domein herontwerpen is een project (programma) op zich en is vele malen te omvangrijk voor de doelstelling van de proof of concept van Uit betrouwbare bron. Voor de proof of concept wordt een deel van het registratieproces van een WOZ-object als scope genomen. De processen met betrekking tot taxatie en waardevaststelling worden buiten beschouwing gelaten. Het informatiemodel dat is opgesteld is dan ook beperkt tot die objecten die relevant zijn voor het registratieproces. Daarnaast is voor het leggen van relaties naar persoons- en organisatie-registraties de structuur die in het kader van klantinteracties is ontwikkeld overgenomen. Deze wijze van registreren van partijen wordt onderdeel van de proof of concept.

In het informatiemodel is de scope beschreven van de gegevens die we in deze proof of concept gaan vastleggen. Deze scope zal gedurende de "Proof of Concept" nog kunnen wijzigen. We documenteren eventuele wijzgingen op de scope en we documenteren wijzigingen in het informatiemodel ten opzichte van het IMWOZ.

Aangezien deze proof of concept met name de register-aspecten betreft worden de casussen beschreven vanuit de optiek van het vastleggen van binnegekomen gegevens. Gegevens logistiek tussen partijen is geen onderdeel van de proof of concept.