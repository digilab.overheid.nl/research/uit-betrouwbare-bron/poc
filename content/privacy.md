---
title: "Privacyverklaring Uit Betrouwbare Bron"
description: ""
date: 2023-09-18T09:35:34+02:00
---

De Privacy verklaring is in ontwikkeling 

### Cookies

Cookies zijn kleine tekstbestanden die op je apparaat worden geplaatst wanneer je onze website bezoekt. Onze websites maken gebruik van cookies en vergelijkbare technologieën om de functionaliteit en prestaties te verbeteren en om je een gepersonaliseerde ervaring te bieden. Onze website gebruikt geen tracking- of marketingcookies. Voor al deze cookies geldt dat u ze altijd handmatig van je computer kunt verwijderen. 

