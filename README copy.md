# Uit Betrouwbare Bron website

[www.uitbetrouwbarebron.nl](https://www.uitbetrouwbarebron.nl)

## Running locally

Make sure Hugo is installed (e.g. `brew install hugo`). Then do:

```sh
pnpm install
hugo server
```

This supports hot reload. The website is then available at http://localhost:1313/


## Running locally via Docker

```sh
docker build -t uit-betrouwbare-bron-website .

docker run --rm --name uit-betrouwbare-bron-website -p 8080:80 uit-betrouwbare-bron-website
```

The website (without hot reload support) is then available at http://localhost:8080/


## Adding docs from other repos

Make sure the other repo has a `docs` dir in the repo root. Then add the repo details to [go.mod](go.mod) and to [config/_default/config.toml](config/_default/config.toml). Then run:

```sh
hugo mod tidy
```

After that, commit the changes to this repo.

To update docs when changed:

```sh
hugo mod get -u
```
