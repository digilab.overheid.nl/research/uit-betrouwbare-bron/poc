---
title: "Over Uit betrouwbare bron"
description: ""
date: 2023-05-23T10:35:34+02:00
---

TODO !!Uitleg over wat we als project willen bewerkstelligen, misschien iest over de planning op hooflijnen en eventuele toelichting op samenhang met andere project / pratijen. Hierbij kan eventueel verwezen worden naar :

[Federatief Datastelsel](https://federatief.datastelsel.nl/). Met standaarden en afspraken over gegevensuitwisseling, waarbij data zoveel mogelijk bij de bron blijft. 

[Generieke Digitale Infrastructuur](https://www.digitaleoverheid.nl/mido/generieke-digitale-infrastructuur-gdi/)

[Interbestuurlijke Datastrategie en het Federatief Datastelsel](https://realisatieibds.pleio.nl/)

[Regie op Gegevens](https://www.digitaleoverheid.nl/overzicht-van-alle-onderwerpen/regie-op-gegevens/)

[Data bij de Bron](https://www.digitaleoverheid.nl/data-bij-de-bron/)


