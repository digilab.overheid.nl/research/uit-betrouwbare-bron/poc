---
layout: page-with-side-nav
title: Basisterminologie 
date: 06-05-2024
---

# Basisterminologie 

Deze pagina beschrijft een toelichting bij kernconcepten die worden erkend. Deze toelichting dient als inleiding tot en aanvulling bij het [semantisch informatiemodel](informatiemodel/informatiemodel.md).