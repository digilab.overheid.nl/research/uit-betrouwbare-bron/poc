# Proof of Concept op basis van de WOZ

Om de inzichten die we in het project "Uit betrouwbare bron" opdoen te toetsen voeren we parrallel aan het opdoen van die inzichten een Proof of Concept uit die gebaseerd is op een praktijksituatie. Deze PoC wordt gedaan op het WOZ-domein. 

## Scope
Voor de Proof of Concept gaan we een deel van het registratieproces van een WOZ-object beproeven.
De taxatie en waardevaststelling-processen worden buiten beschouwing gelaten in de PoC. Het Informatiemodel dat nu is opgesteld is dan ook beperkt tot die objecten die relevant zijn voor het registratieproces.
Daarnaast is voor het leggen van relaties naar persoons- en organisatie-registraties de structuur die in het kader van klantinteracties is ontwikkeld overgenomen. Deze wijze van registreren van partijen wordt onderdeel; van de Proof of Concept. 

In het [informatiemodel](./informatiemodel/informatiemodel) is de scope beschreven van de gegevens die we in deze PoC gaan vastleggen. Deze scope zal gedurende de proof of concept nog kunnen wijzigen. We documenteren eventuele wijzgingen op de scope. 

De causïstiek die we gaan beproeven heeft hoofdzakelijk te maken met de vastlegging van gegeven. We beschrijven een aantal [casussen](./achtergronddocumentatie/cases.md) waarmee we zoveel mogelijk registerfunctionaliteit willen beproeven. 