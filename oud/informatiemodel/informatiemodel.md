---
layout: page-with-side-nav
title: Semantisch informatiemodel PoC WOZ
date: 07-06-2024
---

# Semantisch informatiemodel

## Modellering in Enterprise Architect
Binnen VNG Realisatie wordt Enterprise Architect (EA) gebruikt voor de vastlegging van semantische informatiemodellen (SIM). Onderstaand figuur geeft de modellering van het SIM weer zoals deze in EA is gemodelleerd.
Dit is de 1e versie van het Informatiemodel dat is gepubliceerd. De vorige versies, inclusief een opsomming van de wijzigingen is [hier](./semantisch_model_oud.md) te vinden.

<img src="./assets/POCWOZ_overzicht_V001.png" alt="SIM in Enterprise Architect" width="1000"/>