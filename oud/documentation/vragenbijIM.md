# Proof of Concept op basis van de WOZ

Eerste versie van het informatiemodel voor de Proof of Concept met als onderwerp de WOZ is te vinden in POCWOZ_overzicht.png .

## Scope
Voor de Proof of Concept gaan we een deel van het registratieproces van een WOZ-object beproeven.
De taxatie en waardevaststelling-processen worden buiten beschouwing gelaten in de PoC. Het Informatiemodel dat nu is opgesteld is dan ook beperkt tot die objecten die relevant zijn voor het registratieproces.
Daarnaast is voor het leggen van relaties naar persoons- en organisatie-registraties de structuur die in het kader van klantinteracties is ontwikkeld overgenomen. Deze wijze van registreren van partijen wordt onderdeel; van de Proof of Concept. 

## Openstaande vragen.

Bij het opstellen van deze eerst versie zijn er een aantal vragen ontstaan. Deze worden hier geborgd en zullen in verdere ontwerpsessies van aan antwoord voorzien worden. 

### Modelleer-vragen

1.  Voor de volgende objecten moet nog een goede naam verzonnen worden
    - SWOKOZ (Voorstel: RelevanteOppervlakteSluimerendWOZObject). 
    - SWOWSP (Voorstel: Waterschap_SluimerendWOZObject)  
    - WOZKOZ (Voorstel: RelevanteOppervlakteWOZObject). 
    - WOZWSP (Voorstel: Waterschap_WOZObject)

2. De relatie heeftSluimerendObject heeft nu een relatieklasse WOZSWO. Zijn de eigenschappen die in die relatieklasse zijn opgenomen (In onderzoek en tijdvakRelatie) voldoende reden om in het relationele model een apart objecttype in het leven te roepen? 

    - Voor de relatie heefDeelobject met relatieklasse WOZWDO geld een vergelijkbare vraag.

### Functionele vragen

1. T.a.v de WOZPRSVES: Welke functionaliteit wordt hier geboden ??
    - Als de natuurlijk persoon belanghebbende is waarom zou je danvia een vestiging communiceren?
    - Als een niet natuurlijk persoon (organisatie) belanghebbende is, zou je dan niet alle communicatie voor die niet-natuurlijk persoon via dezelfde vestiging (organisatie) willen laten lopen ? 
    - Wat is de meerwaarde om dit per WOZ-object te kunnen laten verschillen ?

2. Wat is het verschil tussen de relaties "isOnderdeelVan" en "heeftDeelObject" die tussen het WOZObject en het WOZDeelobject zijn gelegd. 

3. Wat is het verschil tussen de relaties "isVoor" en "isGecontroleerd" die tussen het WOZobject en de "Controle objectkenmerken" zijn gelegd.

4. Waarom zou een controle van de objectkenmerken bij meerdere WOZ-objecten kunnen horen ? Kan 1 controle over meerdere WOZ-objecten gaan? 

5. Alle enumeraties bestaan uit codes. Is daar een functionele reden voor of kunnen die codes vervangen worden door de echte betekenis? 







