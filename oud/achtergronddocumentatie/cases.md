---
layout: page-with-side-nav
title: Cases
date: 17-06-2024
---

# Cases

## Proof of concept Woz

### Vastleggen WOZ-object n.a.v. mutatie in de Basiregistratie Kadaster

| Code | Omschrijving | 
| :--- | :--- | 
| [C2949](./artefacten/2949) | Registratie WOZ_object n.a.v. splitsing van een perceel (eigendomsoverdracht)  |
| [C8436](./artefacten/8436) | Registreren wijziging eigenaarschap van een perceel | 


### Vastleggen WOZ-object n.a.v. constateringen of meldingen. 

| Code | Omschrijving | 
| :--- | :--- | 
| [C4048](./artefacten/4048) | Registratie WOZObject n.a.v. een constatering  |


