---
layout: page-with-side-nav
title: Uitgangspunten
date: 17-06-2024
---

# Uitgangspunten

## Ten aanzien van de Proof of Concept zelf

-   We beproeven registratie-aspecten van het WOZ-proces om de werking van een register in een praktijk-case te kunnen toetsen.

## Ten aanzien van de WOZ inhoudelijk in het kader van de Proof of concept

-   Een uitgezonderd ongebouwd WOZ-object kan nooit een "gewoon"Woz-object worden. --> Dus als er wat op gebouwd wordt (wat niet uitgezonderd is) dan wordt er een nieuw WOZ-object gecreëerd. (Kan wel van uitgezonderd ongebouw naar uitgezonderd gebouwd wijzigen.

