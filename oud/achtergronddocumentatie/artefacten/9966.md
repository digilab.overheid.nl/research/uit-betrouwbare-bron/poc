---
layout: page-with-side-nav
title: F9966 - Wijzig WOZObject
date: 08-07-2024
---

# F9966 - Wijzig WOZObject

## Functionaliteit

De functie legt de gewijzigde gegevens van een bestaand  `WOZObject` vast.
Bij het wijzigen worden alleen de gewijzigde eigenschappen van het `WOZObject` aangeboden om vastgelegd te worden. 

    - TODO: Bovenstaande uitspraak is een Design Decision. Deze moet nog bekrachtigd worden.

De uniek identificerende eigenschap wordt meegeleverd om het te wijzigen object te idnetificeren.

- Het wozObjectnummer is niet te wijzigen, want dit is een uniek identificerend gegeven.

### Vastleggen van de gewijzigde eigenschappen WOZ-object :

Aangezien alleen de gewijzigde eigenschappen worden aangeboden om vastgelegd te worden zal er dus een selectie van de volgende eigenschappen onderdeel uitmaken van een wijzigings-actie: 

| ***`Objecttype`*** - Eigenschap | Claimtype - expression | Claimtype - nestedExpression | Uniciteit |
| --- | --- | --- | --- |
| **`WOZObject`** | 
| - wozObjectnummer | `<WOZObject>` is geïdentificeerd door `<wozObjectnummer>` |  | |
| - statusWozObject	 | `<WOZObject>` heeft als status `<statusWOZObject>` |  | 1 |
| - gebruikscode	 |  |  |	1 |
| - typeWOZObject	 | `<WOZObject>` wordt getypeerd door `<typeWOZObject>` |  |	1 |
| - functieWOZObject	 |  |  |	1 |
| - typeWOZObjectOptioneel	 |  |  |	0 .. 1 |
| - aanduidingRepeterend	 |  |  |	0 .. 1 |
| - aanduidingInAanbouw	 |  |  |	1 |
| - soortObjectCode	 |  |  |	1 |
| - codeGebouwdOngebouwd	 |  |  |	1 |
| - wozObjectGeometrie	 |  |  |	0 .. 1 |
| - grondoppervlakte	 |  |  |	0 .. 1 |
| - meegetaxeerdeOppervlakteGebouwd	 |  |  |	0 .. 1 |
| - monumentaanduiding	 |  |  |	0 .. 1 |
| - aanwezigheidLift	 |  |  |	0 .. 1 |
| - waardegebied	 |  |  |	0 .. 1 |
| - indicatieLigging	 |  |  |	0 .. 1 |
| - codeOntbrekenNutsvoorzieningen	 |  |  |	0 .. 3 |
| - codeOmzetBelasting	 |  |  |	0 .. 1 |
| - ozbVrijstelling	 |  |  |	1 |
| - aanduidingBouwstroom	 |  |  |	0 .. 1 |
| - groepaanduiding	 |  |  |	0 .. 1 |
| - omschrijving	 |  |  |	0 .. 1 |
| - documentIdentificatie	 |  |  |	0 .. * |
| - inOnderzoek	 |  |  |	0 .. 11 |
| - ingangsdatumObject	 |  |  |	1 |
| - einddatumObject	 |  |  |	1 |
| - verantwoordelijkeGemeente	 |  |  |	1 |
| - aanduidingWOZobject :	 |  |  |	1 |  
| &nbsp;&nbsp; - identificatieNummeraanduiding	 |  |  |	0 .. 1  |
| &nbsp;&nbsp; - woonplaatsnaam	 |  |  |	1 |
| &nbsp;&nbsp; - postcode	 |  |  |	1 |
| &nbsp;&nbsp; - openbareRuimteNaam	 |  |   |	1 |
| &nbsp;&nbsp; - straatnaam	 |  |  |	0 .. 1 |
| &nbsp;&nbsp; - huisnummer	 |  |  |	1 |
| &nbsp;&nbsp; - huisletter	 |  |  |	1 |
| &nbsp;&nbsp; - huisnummertoevoeging	 |  |  |	1 |
| &nbsp;&nbsp; - locatieomschrijving	 |  |  |	1 |
| - inBewerking :	 |  |  |	0 .. * |
| &nbsp;&nbsp; - aanduidingMedewerkerOrganisatie	 |  |  |	1  |
| &nbsp;&nbsp; - redenInBewerking	 |  |  |	1 |
| &nbsp;&nbsp; - statusInBewerking	 |  |  |	1 |
| &nbsp;&nbsp; - heeftBetrekkingOpJaar	 |  |  |	1 .. * |
| &nbsp;&nbsp; - ingangsdatumInBewerking	 |  |  |	1 |
| &nbsp;&nbsp; - einddatumInBewerking	 |  |  |	1 |
| &nbsp;&nbsp; - aantekening	 |  |  |	0 .. 1 | 