---
layout: page-with-side-nav
title: C4048 - Registratie WOZObject n.a.v. een constatering
date: 17-06-2024
---

# C4048 - Registreren WOZObject n.a.v. een constatering

## Inleiding

Het gaat hier om een weiland dat in gebruik is voor agrarische doeleinden. Dat weiland wordt opgedeeld in een deel dat gebruikt blijft worden voor agrarische doeleinden en een deel dat gebruikt gaat worden als bouwkavel om uiteindelijk een woonhuis op te bouwen. Van deze situatie zijn een aantal case-varianten mogelijk. 

Deze case beschrijft de vastlegging van een nieuw `WOZObject` als gevolg van de constatering dat een deel van het weiland in gebruik genomen is als bouwkavel voor een woning. Er is geen kadastrale wijziging geweest, er is geen eigendomsoverdracht geweest.

-   Het originele perceel was gekoppeld aan een `WOZObject` "uitgezonderd ongebouwd" van 10000 m2 met als eigenaar Jan en als gebruiker ook Jan. Het perceel wijzigt niet.
-   Het gekoppelde `WOZObject` "uitgezonderd ongebouwd" wordt afgesloten.
-   Er wordt een nieuw (regulier) `WOZObject` "ongebouwd bouwkavel" opgevoerd met een oppervlakte van 10000 m2   
-   De verdeling tussen het uitgezonderde deel (8000 m2) en het niet-uitgezonderde deel wordt (2000 M2) wordt in de `WOZKOZ` (koppeling naar het perceel) vastgelegd. Alleen de 2000 m2 wordt als bouwkavel geregistreerd als `WOZDeelobject`. 
      
    
Bovenstaande proces wordt bij een gemeente in gang gezet door: 

-   Een constatering in de fysieke wereld, iemand zou dit kunnen melden bij de gemeenten. 
-   Een wijziging bij de BAG (adres is toegekend). 

Je zou verwachten dat een aanvraag bouwvergunning hier ook een rol speelt, maar dat blijkt in het gros van de gemeenten niet zo te zijn. 

N.a.v. de constatering wordt een inlichtingenformulier naar de eigenaar verstuurd om een aantal gegevens op te vragen voor het vastleggen van de WOZ-gegevens. Ook kan er een "feitelijke constatering" gedaan worden (bv op locatie gaan kijken of een luchtfoto beoordelen om de gegevens beschikbaar te krijgen). In deze casus gaan we er van uit dat de benodigde gegevens door de eigenaar via het informatieformulier worden aangeleverd. 

## 1. Informatieformulier verwerken.

Op basis van de aangeleverde gegevens die op het ingevulde informatieformulier ztaan wordt het bijbehorende `WOZObject` opgezocht m.b.v. de functie [F8412 - Zoek WOZObject](./8412)

## 2. WOZObject ophalen. 

- Met behulp van de functie [F8381 - Vraag WOZObject op](./8381) wordt het `WOZObject` opgehaald. 
- Met behulp van de functie [F6415 - Zoek WOZKOZ](./6415) wordt de koppeling met één of meer kadastrale objecten opgehaald. (In dit geval één perceel).
- Met behulp van de functie [F4197 - Zoek Belanghebbende](./4197) worden de koppeling met de `Belanghebbenden` (Eigenaars, gebruikers ...) opgehaald. 

.

    - TODO: Vraag: Moeten hier alle belanghebbenden opgehaald en omgehangen worden ? Lijkt mij wel..... --> 
    - TODO: Vraag: moeten de acties "WOZ_object afsluiten" en "Nieuw WOZObject registreren" in 1 transactie gebeuren omdat er landelijke dekking van `WOZObjecten` moet zijn want of mag er een tijdelijke situatie bestaan dat er geen landelijke dekking is in een gemeentelijk register 

## 3. WOZObject afsluiten 

Het opgehaalde WOZObject wordt afgesloten door de statusWozObject aan te passen.

    - TODO: Vraag: Is code 8 hierboven invullen functioneel iets anders dan de einddatum geldigheid invullen? 
    - TODO: Vraag: En is voor de casus dan het invullen van alleen de einddatum geldigheid dan afdoende ? 
    - TODO: Vraag: Verandert er nog iets bij het afsluiten van een WOZObject ? Worden bv alle verwijzingen naar gerelateerde objecten (bv. `WOZKOZ` , `Belanghebbenden` ) ook beëindigd ? 

- De volgende wijziging wordt vastgelegd met functie [F9966 - Wijzig WOZObject](./9966) :

| ***`Objecttype`*** - Attribuut | Toegekende waarde | Opmerking | 
| --- | --- | --- | 
| **`WOZObject`** | | |
| - statusWozObject | 8 | Waarde 8 betekent "beëindigd" |

## 4 Nieuwe WOZObject registreren

De volgende gegevens worden overgenomen uit het informatieformulier naar het nieuwe WOZObject :

| ***`Objecttype`***  - Attribuut | Toegekende waarde | Opmerking | 
| --- | --- | --- |
| **`WOZObject`** | | |
| - wozObjectnummer | UUID | |
| - statusWozObject | 0 | "actief" | 
| - gebruikscode | 11 | woning met "ruimte voor bedrijfsmatige activiteiten aan huis" | 
| - typeWozObject | 1811 | In de referentielijst typeWozObject is dit het "Bouwkavel" |
| - functieWozObject | Permanente bewoning | |
| - aanduidingInAanbouw | True | |
| - codeGebouwdOngebouwd | O | "Ongebouwd" | 
| - grondoppervlakte | 10000 | | 
| - ozbVrijstelling | 0 | "OZB-waarde gelijk aan WOZ-waarde" | 

    - TODO: Vraag: Welke optionele gegevens worden er vastgelegd n.a.v. een constatering ? 

Het nieuwe WOZ-object wordt geregistreerd met behulp van de functie [F6681 - Registreer WOZObject](./6681)

## 5 Koppelen met perceel.

Er worden 2 koppelingen gelegd tussen het nieuw geregistreerde WOZObject en het perceel van eigenaar Jan met behulp van funcie [F6415 - Registreer WOZKOZ](./6415). 

| ***`Objecttype`***  - Attribuut | Toegekende waarde | Opmerking | 
| --- | --- | --- |
| **`WOZKOZ`** | 
| - wozObjectnummer | Het wozObjectnummer van het bij punt 4 geregistreerde WOZObject | | 
| - kadastraleIdentificatie | De kadastraleIdentificatie uit de bij punt 2 opgehaalde koppeling naar het kadastraal object | |
| - toegekendeOppervlakte | 2000 | Dit is de oppervlakte die aan het bouwkavel is toegekend |
| - meegetaxeerdeOppervlakte |  |  Moet hier een waarde toegekende worden ? |

    - TODO: Vraag: moet er een meegetaxeerdeOppervlakte worden opgegeven? 
    - TODO: Vraag: Er zijn nu wel 2 koppelingen, maar hieraan kan je niet zien welk Perceel gekoppeld is aan het WOZDeelobject

| ***`Objecttype`***  - Attribuut | Toegekende waarde | Opmerking | 
| --- | --- | --- |
| **`WOZKOZ`** | 
| - wozObjectnummer | Het wozObjectnummer van het bij punt 4 geregistreerde WOZObject | | 
| - kadastraleIdentificatie | De kadastraleIdentificatie uit de bij punt 2 opgehaalde koppeling naar het kadastraal object | |
| - toegekendeOppervlakte | 8000 | Dit is de oppervlakte die aan het bouwkavel is toegekend |
| - meegetaxeerdeOppervlakte |  |  Moet hier een waarde toegekende worden ? |

    - TODO: Vraag: moet er een meegetaxeerdeOppervlakte worden opgegeven? 


## 6 Koppelen met Belanghebbende(n).

Aangezien er geen eigendomsoverdracht plaatsgevonden heeft mag verondersteld worden dat de eigenaar van het perceel reeds bekend is als partij. In het geval er ook andere belanghebbende voor het WOZObject geregistreerd zijn worden deze ook gekoppeld aan het nieuw geregistreerde WOZObject.

Voor iedere belanghebbende die gekoppeld was aan het afgesloten WOZObject wordt met [F2821 - Registreer belanghebbende](./2821) een belanghebbende geregistreerd met de volgende waarden.

| ***`Objecttype`***  - Attribuut | Toegekende waarde | Opmerking | 
| --- | --- | --- |
| **`Belanghebbende`** | | |
| - wozObjectnummer | Het wozObjectnummer van het bij punt 4 geregistreerde `WOZObject` | | 
| - partijId | Het partijId van de bij punt 4 opgehaalde `Belanghebbende` | |
| - aanduidingEigenaarGebruiker | De aanduidingEigenaarGebruiker van de bij punt 4 opgehaalde `Belanghebbende` |  |
| - aanduidingAfdelingOfComplex |  De aanduidingAfdelingOfComplex van de bij punt 4 opgehaalde `Belanghebbende` |  |
| - terAttentieVan | De terAttentieVan van de bij punt 4 opgehaalde `Belanghebbende` |  |
| - zakelijkRechtEigenaar | | |
| &nbsp;&nbsp; - aandeel | het aandeel van de bij punt 4 opgehaalde `Belanghebbende` | |
| &nbsp;&nbsp; - aardZakelijkRecht | het aardZakelijkRecht van de bij punt 4 opgehaalde `Belanghebbende` | |
| &nbsp;&nbsp; - identificatieKadastraalObject | de identificatieKadastraalObject van de bij punt 4 opgehaalde `Belanghebbende` | |

    - Todo: Het vastleggen van het correspondentieadres is nog onderwerp van een ontwerpsessie 
    - Todo: Beschrijven van vastleggen van annotaties (op zijn minst inOnderzoek) 
    - Todo: Beschrijven van vastleggen van historie-aspecten. 
    - Todo: Zijn de gegevens in zakelijkRechtEigenaar relevant voor de WOZ ? Het aandeel wel, maar dat is in feite een Kadastergegeven (data bij de bron)


## 7 WOZDeelobject registreren

- Er wordt een WOZDeelobject geregistreerd voor het gedeelte dat als bouwkavel is aangemerkt. 

De volgende gegevens worden overgenomen uit het informatieformulier naar het nieuwe WOZDeelobject.


| Objecttype | Eigenschap | Toegekende waarde | Opmerking | 
| --- | --- | --- | --- |
| WOZDeelobject | wozDeelobjectnummer |  | ? Hoe wordt dit nummer toegekend ? |
| WOZDeelobject | bepaaltAanduiding | True | |
| WOZDeelobject | statusWozObject | 0 | "actief" | 
| WOZDeelobject | typeWozDeelobject | 1811 | In de referentielijst typeWozObject is dit het "Bouwkavel" |
| WOZDeelobject | aanduidingInAanbouw | True | |
| WOZDeelobject | codeWozDeelobject | | In het informatiemodel maar blijkbaar niet meer in gebruik. | 
| WOZDeelobject | bepaaltGegevensTaxatieverslag |  | Hoe wordt deze waarde toegekend  |
| WOZDeelobject | ingangsdatumObject | Overnemen uit formulier |  | 
| WOZDeelobject | einddatumObject |  | Is volgens informatiemodel verplicht. Lijkt mij niet correct |
| WOZDeelobject | bouwjaar |  | Dit is een keuze veld binnen bouwjaarOfBouwjaarKlasse | 
| WOZDeelobject | bouwjaarKlasse |  | Dit is een keuze veld binnen bouwjaarOfBouwjaarKlasse |

    - TODO: Vraag: Hoe wordt bepaald wat er in de keuze bouwjaarOfBouwjaarKlasse ingevuld wordt ?
    - TODO: Er is nu een Relatieklasse WOZWDO gedefinieerd om de geldigheidstijd van de relatie te kunnen registreren. Graag een ontwerp sessie om te bepalen hoe we hier met ons prototype mee omgaan. 
