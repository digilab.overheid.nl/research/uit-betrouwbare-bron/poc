---
layout: page-with-side-nav
title: C8436 - Registreren wijziging eigenaarschap van een perceel
date: 11-07-2024
---

# C8436 - Registreren wijziging eigenaarschap van een perceel.

## Inleiding

Het gaat hier om een weiland dat in gebruik is voor agrarische doeleinden. Dat weiland wordt verkocht door eigenaar Jan een nieuwe eigenaar Piet. 

- Er is een `WOZObject` "uitgezonderd ongebouwd" van 10000 M2 met als eigenaar Jan en als gebruiker ook Jan.
- Van het kadaster wordt van het perceel dat gekoppeld is met dit `WOZObject` een wijzging ontvangen waaruit blijkt dat er een nieuwe eigenaar is. 

Deze case beschrijft de vastlegging van een nieuwe eigenaar van het WOZ-object als gevolg van de geregistreerde verkoop bij het Kadaster. 
    -   Het eigenaarschap van Jan wordt beëindigd. 
    -   Het eigenaarschap van Piet wordt vastgelegd. 
    -   Er wordt verondersteld dat Piet reeds bekend is als `Partij` in het WOZ-register. 

Dit proces wordt bij de gemeente in gang gezet omdat er door het Kadaster een wijzigingsbericht wordt aangeleverd (middels de KADMUTVERWERK) waarin de bovenstaande gegevens worden aangeleverd. 

## 1. Bericht KADMUTVERWERK wordt ontvangen.

    - Vraag: is het bericht KADMUTVERWERk onderdeel van de BRK-levering ? Worden er Notificaties ontvangen ? Worden er mutatiebestanden aan de gemeente aangeboden ? Met welke frequentie gebeurt dit ?
    - Wordt het bericht KADMUTVERWERK geautomatiseerd verwerkt of is er tussenkomst van een medewerker voordat de WOZ_objecten aangepast / aangemaakt worden ?

    - TODO: Beschrijf hier (of in een bijlage) de relevante inhoud van het KADMUTVERWERK-bericht. 

## 2. Relevante gegevens ophalen.

### 2.1 WOZKOZ zoeken.

Op basis van de kadastraleIdentificatie worden alle `WOZKOZ`-en opgezocht.

### 2.2 WOZObject ophalen <!-- Check of dit wel nodig is. Je kan ook direct de Belanghebbende ophalen -->

Op basis van het wozObjectnummer wordt met [F8381 - Vraag WOZObject op](./8381) het `WOZObject` opgehaald dat gerelateerd is aan de opgehaalde `WOZKOZ`.    

## 2.3 Belanghebbende(n) ophalen. 

Met behulp van de functie [F4197 - Zoek Belanghebbende](./4197) worden alle `Belanghebbenden` opgehaald die gerelateerd zijn aan het opgehaalde `WOZObject`. 

    - TODO: Vraag: Wordt van de nieuwe eigenaar ook aangegeven of die gebruiker is door het Kadaster ? Of is gebruiker alleen een "WOZ"-term.  

## 2.4 Partij (nieuwe eigenaar) opzoeken.

Met behulp van de functie [F4618 - Zoek Partij](./4618) wordt de partij opgehaald die correspondeert met de nieuwe eigenaar(zakelijk gerechtigde) zoals die is aangeleverd door het Kadaster.

<!-- Hier begint de transactie. Hoe geven we dat aan in documentatie ? -->

## 3 Geldigheid van de belanghebbende (eigenaar) wordt beëindigd met de datum van overdracht

Er wordt geverifieerd of de partij de als `belanghebbende` van het type eigenaar is vastgelegd overeenkomt met de vorige zakelijk gerechtigde zoals die door het kadaster is aangeleverd (Hierbij ga ik er vanuit dat het Kadaster nu een was-wordt aanlevert conform StUF).
Van de belanghebbende die overeenkomt met de vorige zakelijk gerechtigde en in de eigenschap `aanduidingEigenaarGebruiker` de waarde "Eigenaar" heeft wordt de geldigheid beëindigd met ingang van de datum van overdracht van het zakelijk recht met behulp van [F9681 - Beeindig geldigheid Belanghebbende](./9681).

    - TODO: Hier moeten we nog bepalen hoe het beëindigen van de geldigheid in zijn werk gaat.

## 4 Nieuwe eigenaar als belanghebbende vastleggen

Er wordt een nieuwe belanghebbende geregistreerd met functie [F2821 - Registreer belanghebbende](./2821) met de volgende waarden: 

| ***`Objecttype`***  - Eigenschap | Toegekende waarde | Opmerking | 
| --- | --- | --- |
| **`Belanghebbende`** | | |
| - wozObjectnummer | Het wozObjectNummer van het bij 2.2 opgehaalde `WOZObject` | |
| - partijId | Het ID van de bij 2.4 opgehaalde `Partij`  | | 
| - aanduidingEigenaarGebruiker | Eigenaar | | 


<!-- Einde van de transactie -->