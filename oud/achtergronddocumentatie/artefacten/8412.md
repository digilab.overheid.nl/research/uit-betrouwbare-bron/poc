---
layout: page-with-side-nav
title: F8412 - Zoek WOZ-Object
date: 08-07-2024
---

# F8412 - Zoek WOZObject 

## Functionaliteit

Deze functie retourneert alle `WOZObjecten` die aan de opgegeven zoekcriteria voldoen.

## API-call voor het zoeken op basis van eigenschappen van de `WOZObject` 

    - TODO: Hoe we het lezen voor schrijven vorm gaan geven is nog niet besloten. Er is dus nog weinig te zeggen over welke call er plaats gaat vinden.

## Zoekargumenten 

    - TODO: Vraag: Welke zoekargumenten worden over het algemeen gebruikt op WOZObjecten te selecteren. 
    - TODO: Moeten we hier onderscheid maken tussen de meest gebruikte (en dus geïndexeerde) en de minder gebruikte ? 

| ***`Objecttype`*** - Attribuut | Voorbeeld waarde | Toelichting |
| :----------- | :----------- | :----------- |
| **`WOZObject`** | 
| - statusWozObject	 | | |
| - gebruikscode	 | | |
| - typeWOZObject	 | | | 
| - functieWOZObject	 | | |
| - typeWOZObjectOptioneel | | |
| - aanduidingRepeteren  | | |
| - aanduidingInAanbouw	 | | |
| - soortObjectCode	 | | |
| - codeGebouwdOngebouwd	 | | | 
| - WOZObjectGeometrie	 | | |
| - grondoppervlakte	 | | |
| - meegetaxeerdeOppervlakteGebouwd	 | | | 
| - monumentaanduiding	 | | |
| - aanwezigheidLift	 | | |
| - waardegebied	 | | |
| - indicatieLigging	 | | | 
| - codeOntbrekenNutsvoorzieningen	 | | |
| - codeOmzetBelasting	 | | |
| - ozbVrijstelling	 | | |
| - aanduidingBouwstroom | | | 
| - groepaanduiding	 | | |
| - omschrijving	 | | | 
| - documentIdentificatie | | | 
| - inOnderzoek	 | | |
| - ingangsdatumObject | | |
| - einddatumObject	 | | | 
| - verantwoordelijkeGemeente | | | 
| - aanduidingWOZobject : | | | 
| &nbsp;&nbsp; - identificatieNummeraanduiding	 | | | 
| &nbsp;&nbsp; - woonplaatsnaam | | |
| &nbsp;&nbsp; - postcode	 | | | 
| &nbsp;&nbsp; - openbareRuimteNaam | | | 
| &nbsp;&nbsp; - straatnaam | | | 
| &nbsp;&nbsp; - huisnummer | | | 
| &nbsp;&nbsp; - huisletter | | | 
| &nbsp;&nbsp; - huisnummertoevoeging | | | 
| &nbsp;&nbsp; - locatieomschrijving | | | 
| - inBewerking : | | | 
| &nbsp;&nbsp; - aanduidingMedewerkerOrganisatie | | | 
| &nbsp;&nbsp; - redenInBewerking | | | 
| &nbsp;&nbsp; - statusInBewerking	| | |
| &nbsp;&nbsp; - heeftBetrekkingOpJaar	| | | 
| &nbsp;&nbsp; - ingangsdatumInBewerking |  | |  
| &nbsp;&nbsp; - einddatumInBewerking | | | 
| &nbsp;&nbsp; - aantekening | | |  

## Response bij het zoeken op basis van eigenschappen van de `WOZObject`

    - TODO: Ook hier weten we nog niet hoe we het lezen voor schrijven gaan vormgeven

De response wordt inhoudelijk beschreven. Houd er rekening mee dat het zoeken op een eigenschap van de `WOZObject` kan resulteren in meerdere `WOZObjecten` in de response. 

| ***`Objecttype`*** - Attribuut | Voorbeeld waarde | Toelichting |
| :----------- | :----------- | :----------- |
| **`WOZObject`** | | |
| - wozObjectnummer | | |
| - statusWozObject	 | | |
| - gebruikscode	 | | |
| - typeWOZObject	 | | | 
| - functieWOZObject	 | | |
| - typeWOZObjectOptioneel | | |
| - aanduidingRepeteren  | | |
| - aanduidingInAanbouw	 | | |
| - soortObjectCode	 | | |
| - codeGebouwdOngebouwd	 | | | 
| - WOZObjectGeometrie	 | | |
| - grondoppervlakte	 | | |
| - meegetaxeerdeOppervlakteGebouwd	 | | | 
| - monumentaanduiding	 | | |
| - aanwezigheidLift	 | | |
| - waardegebied	 | | |
| - indicatieLigging	 | | | 
| - codeOntbrekenNutsvoorzieningen	 | | |
| - codeOmzetBelasting	 | | |
| - ozbVrijstelling	 | | |
| - aanduidingBouwstroom | | | 
| - groepaanduiding	 | | |
| - omschrijving	 | | | 
| - documentIdentificatie | | | 
| - inOnderzoek	 | | |
| - ingangsdatumObject | | |
| - einddatumObject	 | | | 
| - verantwoordelijkeGemeente | | | 
| - aanduidingWOZobject : | | | 
| &nbsp;&nbsp; - identificatieNummeraanduiding	 | | | 
| &nbsp;&nbsp; - woonplaatsnaam | | |
| &nbsp;&nbsp; - postcode	 | | | 
| &nbsp;&nbsp; - openbareRuimteNaam | | | 
| &nbsp;&nbsp; - straatnaam | | | 
| &nbsp;&nbsp; - huisnummer | | | 
| &nbsp;&nbsp; - huisletter | | | 
| &nbsp;&nbsp; - huisnummertoevoeging | | | 
| &nbsp;&nbsp; - locatieomschrijving | | | 
| - inBewerking : | | | 
| &nbsp;&nbsp; - aanduidingMedewerkerOrganisatie | | | 
| &nbsp;&nbsp; - redenInBewerking | | | 
| &nbsp;&nbsp; - statusInBewerking	| | |
| &nbsp;&nbsp; - heeftBetrekkingOpJaar	| | | 
| &nbsp;&nbsp; - ingangsdatumInBewerking |  | |  
| &nbsp;&nbsp; - einddatumInBewerking | | | 
| &nbsp;&nbsp; - aantekening | | |  