---
layout: page-with-side-nav
title: F6514 - Zoek WOZKOZ (koppeling tussen WOZObject en Kadastraal Object).
date: 08-07-2024
---

# F6514 - Zoek WOZKOZ 

## Functionaliteit

Deze functie retourneert alle `WOZKOZ` die aan de opgegeven zoekcriteria voldoen.

## API-call voor het zoeken op basis van eigenschappen van de `WOZObject` 

    - TODO: Hoe we het lezen voor schrijven vorm gaan geven is nog niet besloten. Er is dus nog weinig te zeggen over welke call er plaats gaat vinden.

## Zoekargumenten 

De `WOZKOZ` fungeert als koppeling tussen de `Kadastraal onroerende zaak` en het `WOZObject`. 
Dit object zal met name bevraagd worden doordat van één van de de twee (of allebei) de gerelateerde objecten waarde van de uniek identificerende eigenschap bekend is.  

| ***`Objecttype`*** - Attribuut | 
| --- | 
| **`WOZKOZ`** | 
| - kadastraleIdentificatie | 
| - wozObjectnummer |  

## Response bij het zoeken op basis van eigenschappen van de `WOZKOZ`

    - TODO: Ook hier weten we nog niet hoe we het lezen voor schrijven gaan vormgeven\

| ***`Objecttype`*** - Attribuut | 
| --- | 
| **`WOZKOZ`** | 
| - kadastraleIdentificatie | 
| - wozObjectnummer |  
| - togekendeOppervlakte | 
| - meegetaxeerdeOppervlakte |  

    -   Todo: Hoe annotaties of tijdsaspecten worden geretourneerd moet nog worden uitgewerkt.