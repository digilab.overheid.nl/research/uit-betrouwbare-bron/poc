---
layout: page-with-side-nav
title: F2821 - Registreer belanghebbende
date: 08-07-2024
---

# F2821 - Registreer Belanghebbende

## Functionaliteit

De functie registreert een nieuwe `Belanghebbende`.  

## API-call voor het zoeken op basis van eigenschappen van de `Belanghebbende`

De `Belanghebbende` wordt vastgelegd met per eigenschap de waarde zoals die in de case die deze functie aanroept beschreven is.
Als een eigenschap in de case niet genoemd wordt dan wordt de bijbehorende claim niet vastgelegd. 

### Het vastleggen van het feit dat een 'Belanghebbende bestaat wordt met de volgende claim gedaan:

Er bestaat een `<Belanghebbende>`  

## Vastleggen van de eigenschappen van de `Belanghebbende`

| ***`Objecttype`***  - Eigenschap | Claimtype - expression | Claimtype - nestedExpression | Uniciteit |
| --- | --- | --- | --- | 
| `Belanghebbende`| | | |
| - wozObjectnummer	 | `<Belanghebbende>` heeft een belang in `<WOZObject>` |  |  |  <!-- Dit claimtype moet scherper gemaakt worden. Dit gat over de relatie naar een ander object Hoe formuleer je die --> 
| - partijId | | | |
| - communiceertViaPartijId | | | |
| - aanduidingEigenaarGebruiker | | | |
| - aanduidingAfdelingOfComplex | | | | 
| - terattentieVan | | | |
| - zakelijkRechtEigenaar | | | |
| &nbsp;&nbsp; - aandeel	 | | | |
| &nbsp;&nbsp; - aardZakelijkRecht | | | |
| &nbsp;&nbsp; - identificatieKadastraalObject | | |

    - TODO: correspondentieAdresWoz gaan we voor de POC anders modelleren (dan in de IMWOZ)  via het Partij-model
    - TODO: InOnderzoek (annotatie) en tijdvakRelatie verdienen nig aandacht in een ontwerpsessie.
    - TODO: is de identificatieKadastraalOnroerendeZaak iets anders dan de kadastraleIdentificate ? 