---
layout: page-with-side-nav
title: Functies voor WOZ Proof of concept
date: 17-06-2024
---

# Functies

## WOZ-Objecten

| Code - Naam | Hoofdresource | Gerelateerde resources | Opmerkingen |
| :--- | :--- | :--- | :--- |
| [F6681 - Registreer WOZObject](./artefacten/6681) | `WOZObject` |  |
| [F8412 - Zoek WOZObject](./artefacten/8412) | `WOZObject`| |  
| [F8381 - Vraag WOZObject op](./artefacten/8381) | `WOZObject` |  |
| [F9966 - Wijzig WOZObject](./artefacten/9966) | `WOZObject` |  |  

## Partijen

| Code - Naam | Hoofdresource | Gerelateerde resources | Opmerkingen |
| :--- | :--- | :--- | :--- |
| [F2976 - Registreer partij](./artefacten/2976) | `Partij` | `Partij-identificator` |
| [F4681 - Zoek partij](./artefacten/4681) | `Partij` | `Digitaal adres`, `Partij-identificator` | |
| [F???? - Vraag partij op](./artefacten/????) | `Partij` | `Digitaal adres`, `Partij-identificator` |
| [F???? - Wijzig partij](./artefacten/????) | `Partij` |  |


## Belanghebbenden

| Code - Naam | Hoofdresource | Gerelateerde resources | Opmerkingen |
| :--- | :--- | :--- | :--- |
| [F2821 - Registreer belanghebbendej](./artefacten/2821) | `Belanghebbende` | `WOZObject` , `Partij` |
| [F4197 - Zoek Belanghebbende](./artefacten/4197) | `Belanghebbende` |  |  

## WOZKOZ (Koppeling tussen WOZObject en kadastraal onroerende zaak).

| Code - Naam | Hoofdresource | Gerelateerde resources | Opmerkingen |
| :--- | :--- | :--- | :--- |
| [F6415 - Zoek WOZKOZ](./artefacten/6415) | `WOZKOZ` | `WOZObject` , `Kadastraal Onroerende Zaak` |